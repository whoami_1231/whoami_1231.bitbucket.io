
var vm = new Vue({
    el: '#app',
    name: 'App',
    data:  {
        runs: {},
        heroes: {
            hero_1: {
                name: "Evangelo",
                color: {
                    hsl: "hsl(28.1, 90.2%, 48.2%)",
                    hex: "#f06d06"
                },
                image: 'Evangelo.png',
                selected: true
            },
            hero_2: {
                name: "Walker",
                color: {
                    hsl: "hsl(120, 100%, 25%)",
                    hex: "#008000"
                },
                image: 'Walker.png',
                selected: false
            },
            hero_3: {
                name: "Holly",
                color: {
                    hsl: "hsl(333, 90%, 59%)",
                    hex: "#F5388CC3"
                },
                image: 'Holly.png',
                selected: false
            },
            hero_4: {
                name: "Hoffman",
                color: {
                    hsl: "hsl(0, 89%, 57%)",
                    hex: "#F32F2FC3"
                },
                image: 'Hoffman.png',
                selected: false
            },
            hero_5: {
                name: "Doc",
                color: {
                    hsl: "hsl(219, 89%, 57%)",
                    hex: "#2F74F3D3"
                },
                image: 'Doc.png',
                selected: false
            },
            hero_6: {
                name: "Jim",
                color: {
                    hsl: "hsl(265, 76%, 51%)",
                    hex: "#7225E1D3"
                },
                image: 'Jim.png',
                selected: false
            },
            hero_7: {
                name: "Karlee",
                color: {
                    hsl: "hsl(57, 76%, 51%)",
                    hex: "#E1D825D3"
                },
                image: 'Karlee.png',
                selected: false
            },
            hero_8: {
                name: "Mom",
                color: {
                    hsl: "hsl(239, 86%, 49%)",
                    hex: "#1114EAD3"
                },
                image: 'Mom.png',
                selected: false
            }
        },
        maps: {

            // 1.1
            evans_a: {
                name: "Resurgence",
                act: "Act 1: The Devil's Return",
            },
            evans_b: {
                name: "Tunnel of Blood",
                act: "Act 1: The Devil's Return",
            },
            evans_c: {
                name: "Pain Train",
                act: "Act 1: The Devil's Return",
            },
            evans_d: {
                name: "The Crossing",
                act: "Act 1: The Devil's Return",
            },


            // 1.2
            finley_rescue_a: {
                name: "A Clean Sweep",
                act: "Act 1: Search and Rescue",
            },
            finley_rescue_b: {
                name: "Book Worms",
                act: "Act 1: Search and Rescue",
            },
            finley_rescue_c: {
                name: "Bar Room Blitz",
                act: "Act 1: Search and Rescue",
            },

            // 1.3
            finley_diner_a: {
                name: "Special Delivery",
                act: "Act 1: The Dark Before the Dawn",
            },
            finley_diner_b: {
                name: "The Diner",
                act: "Act 1: The Dark Before the Dawn",
            },

            // 1.4
            bluedog_a: {
                name: "Bad Seeds",
                act: "Act 1: Blue Dog Hollow",
            },
            bluedog_b: {
                name: "Hell's Bells",
                act: "Act 1: Blue Dog Hollow",
            },
            bluedog_c: {
                name: "Abandoned",
                act: "Act 1: Blue Dog Hollow",
            },
            bluedog_d: {
                name: "The Sound of Thunder",
                act: "Act 1: Blue Dog Hollow",
            },



            // 2.1
            finley_police_a: {
                name: "A Call to Arms",
                act: "Act 2: The Armory",
            },
            finley_police_b: {
                name: "The Handy Man",
                act: "Act 2: The Armory",
            },

            // 2.2
            clog_a: {
                name: "Pipe Cleaners",
                act: "Act 2: Plan B",
            },
            clog_b: {
                name: "Hinterland",
                act: "Act 2: Plan B",
            },
            clog_c: {
                name: "Trailer Trashed",
                act: "Act 2: Plan B",
            },
            clog_d: {
                name: "The Clog",
                act: "Act 2: Plan B",
            },
            clog_e: {
                name: "The Broken Bird",
                act: "Act 2: Plan B",
            },

            // 2.3
            finley_church_a: {
                name: "Heralds of the Worm Part 1",
                act: "Act 2: Job 10:22",
            },
            finley_church_b: {
                name: "Heralds of the Worm Part 2",
                act: "Act 2: Job 10:22",
            },
            finley_church_c: {
                name: "Grave Danger",
                act: "Act 2: Job 10:22",
            },


            // 3.1
            manor_a: {
                name: "Farther Afield",
                act: "Act 3: Dr. Rogers Neighborhood",
            },
            manor_b: {
                name: "Blazing Trails",
                act: "Act 3: Dr. Rogers Neighborhood",
            },
            manor_c: {
                name: "Cabins by the Lake",
                act: "Act 3: Dr. Rogers Neighborhood",
            },
            manor_d: {
                name: "Garden Party",
                act: "Act 3: Dr. Rogers Neighborhood",
            },
            manor_e: {
                name: "T5",
                act: "Act 3: Dr. Rogers Neighborhood",
            },

            // 3.2
            cdc_a: {
                name: "A Friend in Need",
                act: "Act 3: Remnants",
            },
            cdc_b: {
                name: "Making the Grade",
                act: "Act 3: Remnants",
            },
            cdc_c: {
                name: "The Road to Hell",
                act: "Act 3: Remnants",
            },
            cdc_d: {
                name: "The Body Dump",
                act: "Act 3: Remnants",
            },

            // 4
            titan_a: {
                name: "The Abomination ",
                act: "Act 4: The Abomination",
            },
        }
    },
    computed: {
        hasData: function() {
            return Object.keys(this.runs).length > 0;
        },
        disabledDeselect() {
            let z = this.heroes;
            for (const [, val] of Object.entries(z)) {
                if (val.selected) {
                    return false;
                }
            }

            return true;
        },
        disabledSelect() {
            let z = this.heroes;
            for (const [, val] of Object.entries(z)) {
                if (!val.selected) {
                    return false;
                }
            }

            return true;
        },
    },
    methods: {
        selectAll() {
            for (const [index, val] of Object.entries(this.heroes)) {
                val.selected = true;
                this.$set(this.heroes, index, val)
            }
            this.$forceUpdate()
        },
        deselectAll() {
            for (const [index, val] of Object.entries(this.heroes)) {
                val.selected = false;
                this.$set(this.heroes, index, val)
            }
            this.$forceUpdate()
        },

        activeHero(hero) {
            return this.heroes[hero].selected;
        },

        toggleActive(hero) {
            let obj = this.heroes[hero]
            obj.selected = !this.heroes[hero].selected
            this.$set(this.heroes, hero, obj);
        },
        getColor(hero) {
          return this.heroes[hero].color.hex;
        },
        buildStyle(map) {

            let style = `box-shadow:`;
            let added = 0;
            for (const [hero, val] of Object.entries(this.heroes)) {
                if (val.selected && Object.prototype.hasOwnProperty.call(this.runs, hero) && Object.values(this.runs[hero]).indexOf(map) > -1) {
                    style = style + (added > 0 ? ',' : '') + " 0 0 0 5px #c4c4c4";
                    added++
                    break;
                }
            }
            if (added === 0) {
                return "";
            }
            return style + ";";
        },
        parseFile(ev) {

            const file = ev.target.files[0];
            const reader = new FileReader();

            if (!ev.target.files[0].name.includes(".sav")) {
                alert("File extension must be .sav");
            }

            reader.onload = function(e) {
                let json = JSON.parse(e.target.result);
                let regex = /(\w+)::hard::(\w+)/;
                for (const [key] of Object.entries(json['onlineDataCache']['stats']['missionsCompleted_Secured']['keys'])) {
                    let matches = key.match(regex)
                    if (matches) {
                        let player = matches[2];
                        let map = matches[1];
                        if (!Object.prototype.hasOwnProperty.call(vm.runs, player)) {
                            vm.$set(vm.runs, player, [])
                        }
                        vm.runs[player].push(map)
                    }
                }
                if (Object.keys(vm.runs).length === 0) {
                    alert("Something wrong with your file, nothing loaded.")
                }
            }
            reader.readAsText(file);
        },
    }})
